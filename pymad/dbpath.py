class dbpath:

    def __init__(self, url):
        self.url = url

    @classmethod
    def sql_server_windows(cls, server, database):
        url_template = '''mssql+pyodbc://{server_name}/{database_name}?driver=SQL+Server+Native+Client+11.0'''
        url = url_template.format(server_name=server, database_name=database)
        return cls(url)

    @classmethod
    def sql_server_osx(cls, server, port, database, username, password):
        url_template = '''mssql+pyodbc://{username}:{password}@{server_name}:{port}/{database_name}?driver=FreeTDS'''
        url = url_template.format(username=username, password=password, port=port, server_name=server, database_name=database)
        return cls(url)
