from sqlalchemy import create_engine, text
import pandas as pd
from pymad import dbpath

class db:

	def read_sql_query(sql, dbpath):
		engine = create_engine(dbpath.url)
		connection=engine.connect()
		try:
			return pd.read_sql_query(sql,con=connection)
		finally:
			connection.close()
			engine.dispose()

	def execute(sql, dbpath):
	    engine = create_engine(dbpath.url)
	    try:
	        return engine.execute(sql)
	    finally:
	        engine.dispose()
