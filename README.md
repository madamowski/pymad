# pymad

## Install directly from github

- Add to requirements.txt

- Specify branch name (master):
`git+https://gitlab.com/madamowski/pymad.git@master#egg=pymad`

- Specify tag (0.0.1.0):
`git+https://gitlab.com/madamowski/pymad.git@0.0.1.0#egg=pymad`
