from setuptools import setup

setup(name='pymad',
      version='0.0.1.0',
      description='MAD python helpers library',
      url='https://gitlab.com/madamowski/pymad.git',
      author='Marcin Adamowski',
      packages=['pymad'],
      install_requires=['sqlalchemy','pandas'],
      license='MIT')
